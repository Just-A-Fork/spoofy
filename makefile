CC = g++
CFLAGS = -I/usr/include/python2.7 -I/usr/include/gstreamer-1.0 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -pthread -lpython2.7 -lgstreamer-1.0 -lgobject-2.0 -lglib-2.0 
DFLAGS = -g

run: compile/production/main
	./$< hello

compile/production/main: src/main.cpp
	$(CC) $< -o $@ $(CFLAGS)

debug: compile/debug/main
	gdb $<

compile/debug/main: src/main.cpp
	$(CC) $(DFLAGS) $< -o $@ $(CFLAGS)
