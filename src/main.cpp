#include<Python.h>
#include<string>
#include<iostream>
#include<gst/gst.h>

std::string getBinLocation(char *argv[]);
std::string getSongUri(std::string query);
bool play(std::string uri);
static gboolean handleAudio(GstBus *bus, GstMessage *msg, GMainLoop *loop);
static gboolean handleKeyboard(GIOChannel *source, GIOCondition cond, GMainLoop *loop);

int main(int argc, char *argv[]){
	gst_init(&argc,&argv);

	if(argc == 1){
		std::cout << "please give query argument" << std::endl;
		return 1;
	}else{
		std::string query;
		for(int i = 1; i < argc; i++){
			query+= std::string(argv[i]) + " ";
		}
		std::string uri = getSongUri(query);
		std::cout << "found song, playing..." << std::endl;
		if(!play(uri)){
			std::cout << "failed to play song" << std::endl;
		}
	}
}

static gboolean handleKeyboard(GIOChannel *source, GIOCondition cond, GMainLoop *loop){
	gchar *str = NULL;

	if(g_io_channel_read_line(source, &str, NULL, NULL, NULL) == G_IO_STATUS_NORMAL){
		if(strcmp(str, "stop")){
			g_main_loop_quit(loop);
		}
	}
}

static gboolean handleAudio(GstBus *bus, GstMessage *msg, GMainLoop *loop){
	switch(GST_MESSAGE_TYPE(msg)){
		case GST_MESSAGE_ERROR:
			g_printerr("Error playing noise");
			break;
		case GST_MESSAGE_EOS:
			g_main_loop_quit(loop);
			break;
	}
}

bool play(std::string uri){
	GstElement *playbin;
	GstBus *bus;
	GstStateChangeReturn ret;
	GIOChannel *ioStdIn;
	GMainLoop *mainLoop = g_main_loop_new(NULL, FALSE);

	playbin = gst_element_factory_make("playbin", "playbin");
	if(!playbin){
		g_printerr("Failed to ready audio");
		return false;
	}

	g_object_set(playbin, "uri", uri.c_str(), NULL);
	g_object_set(playbin, "flags", 0b010, NULL);
	g_object_set(playbin, "connection-speed", 56, NULL);

	bus = gst_element_get_bus(playbin);
	gst_bus_add_watch(bus, (GstBusFunc)handleAudio, mainLoop);

	ioStdIn = g_io_channel_unix_new(fileno(stdin));
	g_io_add_watch(ioStdIn, G_IO_IN, (GIOFunc)handleKeyboard, mainLoop);

	ret = gst_element_set_state(playbin, GST_STATE_PLAYING);
	if(ret == GST_STATE_CHANGE_FAILURE){
		g_printerr("Unable to play audio");
		gst_object_unref(playbin);
		return false;
	}

	g_main_loop_run(mainLoop);

	g_main_loop_unref(mainLoop);
	gst_object_unref(bus);
	gst_element_set_state(playbin, GST_STATE_NULL);
	gst_object_unref(playbin);
	return true;
}

std::string getBinLocation(char *argv[]){
	char* path = argv[0];
	for(int i = strlen(path); i >= 0; i--){
		if(path[i] == '/'){
			path[i+1] = '\0';
			return path;
		}
	}
}

std::string getSongUri(std::string query){
	Py_Initialize();


	std::string python = 
"import sys, os\n\
sys.path.append(\"/usr/share/spoofy/libs/youtube-dl/\")\n\
import youtube_dl\n\
\n\
class StdOutCatch:\n\
	def __init__(self):\n\
		self.caught = ''\n\
	def write(self, txt):\n\
		self.caught += txt\n\
stdOutCatch = StdOutCatch()\n\
sys.stdout = stdOutCatch\n\
\n\
sys.argv = [None, \"-f\", \"140\", \"-g\", \"--no-playlist\", \"ytsearch:" + query + "\"]\n\
try:\n\
	youtube_dl.main()\n\
except:\n\
	pass";

	PyObject *PyModuleMain = PyImport_AddModule("__main__");
	PyRun_SimpleString(python.c_str());
	PyObject *PyCatcher = PyObject_GetAttrString(PyModuleMain, "stdOutCatch");
	PyObject *PyCaught = PyObject_GetAttrString(PyCatcher, "caught");
	
	return PyString_AsString(PyCaught);
}
